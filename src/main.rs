extern crate i3ipc;
extern crate regex;

use i3ipc::event::Event;
use i3ipc::I3EventListener;
use i3ipc::I3Connection;
use i3ipc::Subscription;
use regex::Regex;

fn main() {
    let mut listener = I3EventListener::connect().expect("failed to set event listener up.");
    let mut connection = I3Connection::connect().expect("failed to connect.");

    let path_re = Regex::new(r"[\w-]+@[\w-]+: (.*)").unwrap();
    let command_re = Regex::new(r"exec-in-folder (.*)$").unwrap();
    let mut path = "~".to_string();

    listener
        .subscribe(&[Subscription::Window, Subscription::Binding])
        .expect("failed to subscribe");
    for event in listener.listen() {
        match event {
            Ok(Event::WindowEvent(w)) => {
                let name = w.container.name.unwrap_or("a@a: ~".to_string());
                match path_re.captures_iter(&name).next() {
                    Some(cap) => {
                        path = cap[1].to_string();
                    }
                    None => ()
                }
            }

            Ok(Event::BindingEvent(e)) => {
                match command_re.captures_iter(&e.binding.command).next() {
                    Some(cap) => {
                        let command = &cap[1].replace("{{path}}", &path);
                        println!("got exec-in-folder command. Running {}", command);
                        connection.run_command(&format!("exec {}", &command)[..]).unwrap();
                    }
                    None => ()
                }
            }

            Err(e) => println!("error: {}", e),
            _ => unreachable!(),
        }
    }
}
