# i3-open-in

Rust program to open applications in current folder using i3 IPC interface.

Works with terminator, not tested with anything else.

## Usage

To open applications in current folder, change the corresponding "exec" command in i3 config to "exec-in-folder" and specify how the command will use the given path with `{{path}}`.

Run the program and everything should work out of the box.

Example:

`bindsym $mod+n exec-in-folder nautilus {{path}}` will start nautilus in the folder of the last focused terminator.
